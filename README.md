# MAPARE

A script to automate installing manjaro package lists. 

Currently supports the 3 official edition desktops: GNOME, KDE, and XFCE

<br>

```text
 Manjaro Package Restore

 Retrieve and reinstall package lists

 Options:

  -h to show this help

  -I to install missing packages (--needed by default)

 Install options (require -I):

  -A to install the entire package list
  -D to limit package list to desktop profile
  -E to mark packages as explicitly installed
  -P to only print the package list
  -R to reinstall system packages

  Use the additional --overwrite flag to overwrite existing files.
  (Use With Caution)
```

<br>

### Run the script directly from the url

The syntax looks like this:<br>
`bash <(curl -s https://gitlab.com/cscs/mapare/-/raw/main/mapare) {options}`

An example run printing what would be installed:
```text
bash <(curl -s https://gitlab.com/cscs/mapare/-/raw/main/mapare) -IP
```


### Download and use

##### Download
```text
curl -O https://gitlab.com/cscs/mapare/-/raw/main/mapare
```

##### Mark Executable:

```text
chmod +x mapare
```

##### Run:

```text
./mapare -I
```

<br></br>

### Donate  

Everything is free, but you can donate using these:  

[<img src="https://az743702.vo.msecnd.net/cdn/kofi4.png?v=2" width=160px>](https://ko-fi.com/X8X0VXZU) &nbsp;[<img src="https://gitlab.com/cscs/resources/raw/master/paypalkofi.png" width=160px />](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M2AWM9FUFTD52)
